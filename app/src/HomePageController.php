<?php

namespace Main;

use PageController;

use SilverStripe\View\Requirements;
use SilverStripe\ORM\DataObject;

// use SilverStripe\Control\HTTPRequest;

class HomePageController extends PageController
{

	protected function init()
    {
        parent::init();

    }

    //to call this on template use $GetFooter.fieldname based on $db variable of Footer.php in src app/src/
    public function GetFooter(){

		return DataObject::get_one("Footer");

	}


	public function GetCTA(){

		return DataObject::get_one("CTA");

	}

	public function CheckURL_CTA(){

		$cta_global = DataObject::get_one("CTA");

		if( PageController::CheckStringSpaces($this->CTA_home_button_label_redirect_to) ){

			$url = $this->CTA_home_button_label_redirect_to;

		}else if ( PageController::CheckStringSpaces($cta_global->CTA_home_button_label_redirect_to)){

			$url = $cta_global->CTA_home_button_label_redirect_to;


			// AboutPage::get()['record']->fieldname

		}else{

			$url = "#";
		}

		return $url;
	}


	public function CheckBackgroundImage_CTA(){

		$cta_global = DataObject::get_one("CTA");

		if( $this->CTA_home_pic_backgroundID != 0 ){

			$background_image = $this->CTA_home_pic_backgroundID;

		}else if ( $cta_global->CTA_home_pic_backgroundID != 0 ){

			$background_image = $cta_global->CTA_home_pic_backgroundID;

		}else{

			$background_image = "assets2/images/cta-bg.png";

		}

		return $background_image;

	}



}