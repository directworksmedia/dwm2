<?php

namespace {

	// use Page;
	//form fields
	use SilverStripe\Forms\TextField;
	use SilverStripe\Forms\TextareaField;
	use SilverStripe\Forms\HeaderField;

	// image and files fields
	use SilverStripe\Assets\Image;
	use SilverStripe\AssetAdmin\Forms\UploadField;


	use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

	class CTA extends Page{

		private static $db = [
			

			//CTA Section
    		'CTA_home_caption_top' => 'Varchar(47)',
    		'CTA_home_main_caption' => 'Varchar(25)',
    		'CTA_home_button_label' => 'Varchar(15)',
            'CTA_home_button_label_redirect_to' => 'Text',
    		//END OF CTA Section


		];


        private static $has_one = [
    
            //CTA
    		'CTA_home_pic_background' => Image::class,
            //END OF CTA

        ];

		private static $owns = [

			//CTA
    		'CTA_home_pic_background',
            //END OF CTA

	    ];


	    public function getCMSFields() {

			$fields = parent::getCMSFields();



			//CTA SECTION
    		// $fields->addFieldToTab(
    		// 	'Root.Main',
    		// 	new HeaderField(" ",'CTA SECTION',1),
    		// 	'Content'
    		// );
    		
    		$fields->addFieldToTab(
    			'Root.Main',
    		    TextareaField::create('CTA_home_caption_top','Label Top')->setMaxLength(47)->setRows(2)->setDescription('Maximum of 47 characters including spaces.'),
    	        // 'Content'
    	        ''
    	    );
    
    		$fields->addFieldToTab(
    			'Root.Main',
    	   	 	TextField::create('CTA_home_main_caption','Main Label')->setMaxLength(25)->setDescription('Maximum of 25 characters including spaces.'),
    	   	 	// 'Content'
    	   	 	''
    	   	);
    
    		$fields->addFieldToTab(
    			'Root.Main',
    	   	 	TextField::create('CTA_home_button_label','Button Label')->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
    	   	 	// 'Content'
    	   	 	''
    	   	);
    
    
            $fields->addFieldToTab(
                'Root.Main',
                TextField::create('CTA_home_button_label_redirect_to','Redirect To')->setAttribute('placeholder','http://sample.com')->setAttribute('type','url')->setAttribute('pattern','https?://.+')->setAttribute('required','required'),
                // 'Content'
                ''
            );
    		//END OF CTA SECTION


	        //CTA
            $fields->addFieldToTab(
                'Root.Main',
                $CTA_home_pic_background = UploadField::create('CTA_home_pic_background','Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1440px and height: 500px)"),
                // 'Content'
                ''
            );
            $CTA_home_pic_background->getValidator()->setAllowedExtensions(['png','PNG']);
            $CTA_home_pic_background->getValidator()->setMinDimensions(1440,500);
            $CTA_home_pic_background->setAllowedMaxFileNumber(1);
            $CTA_home_pic_background->setFolderName('Uploads/CTA');
            //END OF CTA


			$fields->removeFieldFromTab(
				"Root.Main",
				"Content"
			);

			$fields->removeFieldFromTab(
				'Root.Main',
				'Metadata'
			);
			return $fields; 

		}



	}


}