<?php


namespace Main;

use PageController;

use SilverStripe\View\Requirements;
use SilverStripe\ORM\DataObject;

use SilverStripe\Forms\Form;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\RequiredFields;

use SilverStripe\Control\Director;
use SilverStripe\Control\HTTPRequest;

use SilverStripe\Control\Email\SwiftMailer;
use SilverStripe\Control\Email\Email;
use SilverStripe\Core\Config\Config;

use Contact;


class ContactPageController extends PageController
{

	protected function init()
    {
        parent::init();
        
    }


    public function GetFooter(){

		return DataObject::get_one("Footer");
		
	}

	public function GetCTA(){

		return DataObject::get_one("CTA");

	}


	public function CheckURL_CTA(){

		$cta_global = DataObject::get_one("CTA");

		if( PageController::CheckStringSpaces($this->CTA_home_button_label_redirect_to) ){

			$url = $this->CTA_home_button_label_redirect_to;

		}else if ( PageController::CheckStringSpaces($cta_global->CTA_home_button_label_redirect_to)){

			$url = $cta_global->CTA_home_button_label_redirect_to;


			// AboutPage::get()['record']->fieldname

		}else{

			$url = "#";
		}

		return $url;
	}

	public function CheckBackgroundImage_CTA(){

		$cta_global = DataObject::get_one("CTA");

		if( $this->CTA_home_pic_backgroundID != 0 ){

			$background_image = $this->CTA_home_pic_backgroundID;

		}else if ( $cta_global->CTA_home_pic_backgroundID != 0 ){

			$background_image = $cta_global->CTA_home_pic_backgroundID;

		}else{

			$background_image = "assets2/images/cta-bg.png";

		}

		return $background_image;

	}


	// CONTACT FORM
	private static $allowed_actions = array(
        'submit_contact_query'
    );


    public function getCurrentLink() {
        return Director::get_current_page()->Link()."submit_contact_query";
    }

	public function ContactForm()
	{

		$fields = new FieldList(

		);

		$actions = new FieldList(
			$mySubmit = new FormAction('submit_application', 'Submit')
		);
		

		$form = new Form($this, 'submit_application', $fields, $actions);
		$form->setTemplate('Includes/ContactForm');

		return $form->customise(
			[
		    	'getCurrentLink' => $this->getCurrentLink()
			]
		);

	}
	

	public function submit_contact_query(HTTPRequest $request){

		$post = $request->requestVars();

		// form details
		$name = $post['name']; 
		$email = $post['email'];
		$contact = $post['contact'];
		$message = $post['message'];


	    //********************************************************************

		//							HUBSPOT

		//********************************************************************


				$name_seperate = explode(" ", $name);

				$arr = array(
				            'properties' => array(
				                array(
				                    'property' => 'email',
				                    'value' => $email
				                ),
				                array(
				                    'property' => 'firstname',
				                    'value' => $name_seperate[0]
				                ),
				                array(
				                    'property' => 'lastname',
				                    'value' => $name_seperate[1]
				                ),
				                array(
				                    'property' => 'phone',
				                    'value' => $contact
				                )
				            )
				        );

				        $post_json = json_encode($arr);

				        // hubspot account API key on my account :D "jerome@directworksmedia.com"
				        $hapikey = "4cb4adf5-0fe3-4bd7-866d-1d54c7569bb9";

				        $endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;
				        $ch = @curl_init();
				        @curl_setopt($ch, CURLOPT_POST, true);
				        @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
				        @curl_setopt($ch, CURLOPT_URL, $endpoint);
				        @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				        $response = @curl_exec($ch);
				        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
				        $curl_errors = curl_error($ch);
				        @curl_close($ch);


				    $return_value = [];

				     //succcess
				    if($status_code == 200){

				    	//saving db
						$contact = Contact::create([
							'db_name' => $name,
							'db_email' => $email,
							'db_contact' => $contact,
							'db_message' => $message,
						]);
						$contact->write();



						//********************************************************************

						//							EMAILING

						//********************************************************************

						// sender
						$from = "jerome@directworksmedia.com";

						//main receiver
						$to = "jerome@directworksmedia.com";

						$subject = "Contact Form Mailer";

						$body = "";
						$body .= "Name : ".$name."<br>";
						$body .= "Email : ".$email."<br>";
						$body .= "Contact :".$contact."<br>";
						$body .= "Message : ".$message;
						
						$email = new Email($from, $to, $subject, $body);
						$email->setFrom($from,"Contact Form Mailer");

						// to address multiple email
						// $email->addCC("marc@directworksmedia.com");
						// $email->addCC("dustin@directworksmedia.com");

						// Config::modify()->set(Email::class, 'cc_all_emails_to', "dustin@directworksmedia.com");
						// Config::modify()->set(Email::class, 'bcc_all_emails_to', "marc@directworksmedia.com");
						// Config::modify()->set(Email::class, 'send_all_emails_to', "jerome@directworksmedia.com");

						if(!$email->send()) {

				           	$return_value["valid"] = "email_not_send";

				        } else {

		           			$return_value["valid"] = "success";

				        }


				    }else{

				    	//show the message from hubspot response
				    	$json_decode = json_decode($response);
		           		$return_value["valid"] = $json_decode->message;

				    }


	    			return json_encode($return_value);

	}

	// END OF CONTACT FORM


}