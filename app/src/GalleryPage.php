<?php

use Page;


use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

use SilverStripe\ORM\DataObject;


use Photo;
use GalleryPageController;

class GalleryPage extends Page
{


	private static $db = [

	];

	//so not allowed to create a page in Main Sitetree
	private static $can_be_root = false;


    private static $has_many = [
        'Photo' => Photo::class,
    ];

    private static $owns = [
        'Photo'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();


        // Create TABLE
        $fields->addFieldToTab(
        	'Root.Main',
        	GridField::create('Photo','List of Photo',$this->Photo(),GridFieldConfig_RecordEditor::create()),
        	''
        );


        // remove Static Content & meta data Fields
		$fields->removeFieldFromTab(
			'Root.Main',
			'Content'
		);

		$fields->removeFieldFromTab(
			'Root.Main',
			'Metadata'
		);

        return $fields;
    }

    //call in about controller
    public function GetAboutGallery(){

		$get_gallery = Photo::get()
		// ->filter([
		// 	"GalleryPageID" => 8
		// ])
		->leftJoin("Photo", "Photo.GalleryPageID = SiteTree_Live.ID","SiteTree_Live")
		->sort('Photo.ID ASC');

		if($get_gallery->count()){

			return $get_gallery;

		}else{

			return 0;

		}


	}



}