<?php

namespace Main;

use Page;
//form fields
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\HeaderField;

// image and files fields
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;


use SilverStripe\Forms\HTMLEditor\HTMLEditorField;



class HomePage extends Page 
{
	private static $table_name = 'Main_DefaultPage';

	private static $db = [

		// HEADER SECTION
		'Top_caption_button_contact' => 'Varchar(21)',
		'Top_caption' => 'Varchar(21)',
		'Top_caption_sub' => 'Varchar(47)',
		'Top_caption_button' => 'Varchar(15)',


		// SERVICES SECTION
		'Services_caption' => 'Varchar(21)',

		// design
		'Services_design_caption' => 'Varchar(23)',
		'Services_design_detail' => 'Varchar(100)',

		// strategy
		'Services_strategy_caption' => 'Varchar(23)',
		'Services_strategy_detail' => 'Varchar(100)',

		// development accelerator
		'Services_development_caption' => 'Varchar(23)',
		'Services_development_detail' => 'Varchar(100)',

		// consulting
		'Services_consulting_caption' => 'Varchar(23)',
		'Services_consulting_detail' => 'Varchar(100)',

		// FEATURED WORKS SECTION
		'Featured_works_caption' => 'Varchar(21)',

		'Featured_works_card1_caption' => 'Varchar(21)',
		'Featured_works_card1_detail' => 'Varchar(100)',
		'Featured_works_card1_sub' => 'Varchar(21)',

		'Featured_works_card2_caption' => 'Varchar(21)',
		'Featured_works_card2_detail' => 'Varchar(100)',
		'Featured_works_card2_sub' => 'Varchar(21)',

		'Featured_works_button' => 'Varchar(23)',


		//WHO WE ARE SECTION
		'Who_we_are_caption' => 'Varchar(21)',

		'Who_we_are_detail_1' => 'Text',
		'Who_we_are_caption_2' => 'Varchar(21)',
		'Who_we_are_detail_2' => 'Text',
		'Who_we_are_button' => 'Varchar(21)',


		// OUR APPROACH SECTION
		"Our_approach_caption" => 'Varchar(21)',

		//product discovery
		"Our_approach_product_discovery_caption" => 'Varchar(23)',
		"Our_approach_product_discovery_detail" => 'Varchar(100)',
		"Our_approach_product_list" => 'HTMLText',


		//product planning
		"Product_planning_caption" => 'Varchar(23)',
		"Product_planning_detail" => 'Varchar(100)',
		"Product_planning_list" => 'HTMLText',


		//UI Design
		"UI_design_caption" => 'Varchar(23)',
		"UI_design_detail" => 'Varchar(100)',
		"UI_design_list" => 'HTMLText',
		"UI_design_link_label" => 'Varchar(25)',


		//Product Development Documentation
		"Product_development_documentation_caption" => 'Varchar(23)',
		"Product_development_documentation_detail" => 'Varchar(100)',
		"Product_development_documentation_list" => 'HTMLText',
		//END OF OUR APPROACH SECTION




		// Articles Tiles SECTION

		// card 1
		'Artiles_tiles_card1_details' => 'Varchar(47)',
		'Artiles_tiles_card1_anchor' => 'Varchar(12)',

		// card 2
		'Artiles_tiles_card2_details' => 'Varchar(47)',
		'Artiles_tiles_card2_anchor' => 'Varchar(12)',

		// card 3
		'Artiles_tiles_card3_details' => 'Varchar(47)',
		'Artiles_tiles_card3_anchor' => 'Varchar(12)',

		// End of Articles Tiles SECTION


		//CTA Section
		'CTA_home_caption_top' => 'Varchar(47)',
		'CTA_home_main_caption' => 'Varchar(25)',
		'CTA_home_button_label' => 'Varchar(15)',
		'CTA_home_button_label_redirect_to' => 'Text',
		//END OF CTA Section


		//Footer SECTION
        'Footer_address' => 'Text',
        'Footer_phone' => 'Text',
        'Footer_email' => 'Text',

        'Footer_social_facebook_link' => 'Text',
        'Footer_social_tweeter_link' => 'Text',
        'Footer_social_instagram_link' => 'Text'
        //END OF FOOTER SECTION


	];

	private static $has_one = [

		// HEADER
		'Logo' => Image::class,
		'Header_background' => Image::class,
		// END OF HEADER

		//OUR SERVICES
		'Design_pic' => Image::class,
		'Strategy_pic' => Image::class,
		'Development_accelerator_pic' => Image::class,
		'Consulting_pic' => Image::class,
		//END OF OUR SERVICES

		//Featured Works
		'Featured_works_Card1_pic_background' => Image::class,
		'Featured_works_Card1_pic' => Image::class,
		'Featured_works_Card2_pic_background' => Image::class,
		'Featured_works_Card2_pic' => Image::class,
		//END OF Featured Works


		// Who we are
		'Who_we_are_top_left_pic' => Image::class,
		'Who_we_are_top_right_pic' => Image::class,
		'Who_we_are_bottom_left_pic' => Image::class,
		'Who_we_are_bottom_right_pic' => Image::class,
		//END OF WHO we are


		//Our Approach
		'Our_Approach_pic_1' => Image::class,
		'Our_Approach_pic_2' => Image::class,
		'Our_Approach_pic_3' => Image::class,
		'Our_Approach_pic_4' => Image::class,
		//END of Our Approach


		//article tiles
		'Article_pic_1' => Image::class,
		'Article_pic_2' => Image::class,
		'Article_pic_3' => Image::class,
		//end of article tiles


		//CTA
		'CTA_home_pic_background' => Image::class,
        //END OF CTA


        //Footer Logo
        // 'Footer_Logo' => Image::class


	];

	private static $owns = [
		
		// HEADER
        'Logo',
        'Header_background',
		// END OF HEADER


        // OUR SERVICES
        'Design_pic',
		'Strategy_pic',
		'Development_accelerator_pic',
		'Consulting_pic',
        // END OF OUR SERVICES

        //FEATURED WORKS
		'Featured_works_Card1_pic_background',
		'Featured_works_Card1_pic',
		'Featured_works_Card2_pic_background',
		'Featured_works_Card2_pic',
        //END OF FEATURED WORKS


		// WHO WE ARE
		'Who_we_are_top_left_pic',
		'Who_we_are_top_right_pic',
		'Who_we_are_bottom_left_pic',
		'Who_we_are_bottom_right_pic',
		//END OF WHO WE ARE


		//Our Approach
		'Our_Approach_pic_1',
		'Our_Approach_pic_2',
		'Our_Approach_pic_3',
		'Our_Approach_pic_4',
		//ENd of Our Approach


		//Article Tiles
		'Article_pic_1',
		'Article_pic_2',
		'Article_pic_3',
		//End of Article Tiles

        //CTA
		'CTA_home_pic_background',
        //END OF CTA


        //Footer Logo
		// 'Footer_Logo'
        //END OF Footer Logo



    ];


	public function getCMSFields(){

	    $fields = parent::getCMSFields();


	    // *****************************************

        //					TEXT

        //******************************************

		// HEADER SECTION

		// $fields->addFieldToTab(
		// 	'Root.Header Section',
		// 	new HeaderField(" ",'HEADER SECTION',1),
		// 	'Content'
		// );

		$fields->addFieldToTab(
			'Root.Header Section.Text',
   	 		TextField::create('Top_caption_button_contact',"Top Button Label")->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
   	 		// 'Content'
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
   	 		'Root.Header Section.Text',
   	 		TextField::create('Top_caption','Main Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
   	 		// 'Content'
   	 		''
   	 	);

	    $fields->addFieldToTab(
	    	'Root.Header Section.Text',
	    	TextareaField::create('Top_caption_sub','Sub Label')->setMaxLength(47)->setDescription('Maximum of 47 characters including spaces.')->setRows(2),
        	// 'Content'
        	''
        );

		$fields->addFieldToTab(
			'Root.Header Section.Text',
   	 		TextField::create('Top_caption_button','Main Button Label')->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
   	 		// 'Content'
   	 		''
   	 	);
		// END OF HEADER SECTION



		// SERVICES SECTION
		// $fields->addFieldToTab(
		// 	'Root.Services Section',
		// 	new HeaderField(" ",'SERVICES SECTION',1),
		// 	'Content'
		// );

		$fields->addFieldToTab(
			'Root.Services Section.Text',
	   	 	TextField::create('Services_caption','Main Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		// DESIGN
		$fields->addFieldToTab(
			'Root.Services Section.Text',
			new HeaderField(" ",'Card Top Left',3),
			// 'Content'
			''
		);

		$fields->addFieldToTab(
			'Root.Services Section.Text',
	   	 	TextField::create('Services_design_caption','Label')->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	 );


		$fields->addFieldToTab(
			'Root.Services Section.Text',
		    TextareaField::create('Services_design_detail','Details')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );
		// END OF DESIGN


		// Strategy
		$fields->addFieldToTab(
			'Root.Services Section.Text',
			new HeaderField(" ",'Card Top Right',3),
			// 'Content'
			''
		);

		$fields->addFieldToTab(
			'Root.Services Section.Text',
	   	 	TextField::create('Services_strategy_caption','Label')->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
	   	 	// 'Content',
	   	 	''
	   	);


		$fields->addFieldToTab(
			'Root.Services Section.Text',
		    TextareaField::create('Services_strategy_detail','Details')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );
		// End of  Strategy


		// Development Accelerator
		$fields->addFieldToTab(
			'Root.Services Section.Text',
			new HeaderField(" ",'Card Bottom Left',3),
			// 'Content'
			''
		);

		$fields->addFieldToTab(
			'Root.Services Section.Text',
	   	 	TextField::create('Services_development_caption','Label')->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);


		$fields->addFieldToTab(
			'Root.Services Section.Text',
		    TextareaField::create('Services_development_detail','Details')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );
		// End of Development Accelerator


		// Consulting
		$fields->addFieldToTab(
			'Root.Services Section.Text',
			new HeaderField(" ",'Card Bottom Right',3),
			// 'Content'
			''
		);

		$fields->addFieldToTab(
			'Root.Services Section.Text',
	   	 	TextField::create('Services_consulting_caption','Label')->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);


		$fields->addFieldToTab(
			'Root.Services Section.Text',
		    TextareaField::create('Services_consulting_detail','Details')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );
		// End of Consulting

		// END OF SERVICES SECTION



		// FEATURES WORKS SECTION
		// $fields->addFieldToTab(
		// 	'Root.Main',
		// 	new HeaderField(" ",'FEATURED WORKS SECTION',1),
		// 	'Content'
		// );

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_caption','Main Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);


		//First Board
		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_card1_caption','Card 1 Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
		    TextareaField::create('Featured_works_card1_detail','Card 1 Details')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_card1_sub','Card 1 Link Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);
		//END of First Board


		//Second Board
		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_card2_caption','Card 2 Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
		    TextareaField::create('Featured_works_card2_detail','Card 2 Details')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_card2_sub','Card 2 Link Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);


		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_button','Bottom Button Label')->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);
		//END of Second Board

		// END OF FEATURES WORKS SECTION




		// WHO WE ARE SECTION
		// $fields->addFieldToTab(
		// 	'Root.Main',
		// 	new HeaderField(" ",'WHO WE ARE SECTION',1),
		// 	'Content'
		// );

		$fields->addFieldToTab(
			'Root.Who We Are Section.Text',
	   	 	TextField::create('Who_we_are_caption','Main Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		//Details 1
		$fields->addFieldToTab(
			'Root.Who We Are Section.Text',
		    TextareaField::create('Who_we_are_detail_1','Details 1')->setMaxLength(100)->setRows(2),
	        // 'Content'
	        ''
	    );

		//Sub Label 2
		$fields->addFieldToTab(
			'Root.Who We Are Section.Text',
		    TextField::create('Who_we_are_caption_2','Sub Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		//Details 2
		$fields->addFieldToTab(
			'Root.Who We Are Section.Text',
		    TextareaField::create('Who_we_are_detail_2','Details 2')->setMaxLength(100)->setRows(2),
	        // 'Content'
	        ''
	    );

		//Bottom Button Label
		$fields->addFieldToTab(
			'Root.Who We Are Section.Text',
		    TextField::create('Who_we_are_button','Bottom Button Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	        // 'Content'
	        ''
	    );
		// END OF WHO WE ARE SECTION



		//OUR APPROACH SECTION
		// $fields->addFieldToTab(
		// 	'Root.Main',
		// 	new HeaderField(" ",'OUR APPROACH SECTION',1),
		// 	'Content'
		// );

		$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
	   	 	TextField::create('Our_approach_caption','Main Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		//product discovery
		$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
	   	 	TextField::create('Our_approach_product_discovery_caption','Label 1')->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
		    TextareaField::create('Our_approach_product_discovery_detail','Details 1')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
			new HtmlEditorField('Our_approach_product_list', 'List Details 1'),
			// 'Content'
			''
		);


		//product planning
		$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
	   	 	TextField::create('Product_planning_caption','Label 2')->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

	   	$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
		    TextareaField::create('Product_planning_detail','Details 2')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );

	    $fields->addFieldToTab(
			'Root.Our Approach Section.Text',
			new HtmlEditorField('Product_planning_list', 'List Details 2'),
			// 'Content'
			''
		);


		//UI Design
	    $fields->addFieldToTab(
			'Root.Our Approach Section.Text',
	   	 	TextField::create('UI_design_caption','Label 3')->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

	   	$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
		    TextareaField::create('UI_design_detail','Details 3')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );

	    $fields->addFieldToTab(
			'Root.Our Approach Section.Text',
			new HtmlEditorField('UI_design_list', 'List Details 3'),
			// 'Content'
			''
		);

		$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
	   	 	TextField::create('UI_design_link_label','Link Label 4')->setMaxLength(25)->setDescription('Maximum of 25 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);


		//Product Development Documentation
		$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
	   	 	TextField::create('Product_development_documentation_caption','Label 4')->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

	   	$fields->addFieldToTab(
			'Root.Our Approach Section.Text',
		    TextareaField::create('Product_development_documentation_detail','Details 4')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );

	    $fields->addFieldToTab(
			'Root.Our Approach Section.Text',
			new HtmlEditorField('Product_development_documentation_list', 'List Details 4'),
			// 'Content'
			''
		);
		//END OF OUR APPROACH SECTION



		//Articles Tiles SECTION
		// $fields->addFieldToTab(
		// 	'Root.Main',
		// 	new HeaderField(" ",'ARTICLES TILES SECTION',1),
		// 	'Content'
		// );

		//card 1
		$fields->addFieldToTab(
			'Root.Articles Tiles Section.Text',
		    TextareaField::create('Artiles_tiles_card1_details','Card 1 Label')->setMaxLength(47)->setRows(2)->setDescription('Maximum of 47 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.Articles Tiles Section.Text',
	   	 	TextField::create('Artiles_tiles_card1_anchor','Card 1 Link Label')->setMaxLength(12)->setDescription('Maximum of 12 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		//card 2
		$fields->addFieldToTab(
			'Root.Articles Tiles Section.Text',
		    TextareaField::create('Artiles_tiles_card2_details','Card 2 Label')->setMaxLength(47)->setRows(2)->setDescription('Maximum of 47 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.Articles Tiles Section.Text',
	   	 	TextField::create('Artiles_tiles_card2_anchor','Card 2 Link Label')->setMaxLength(12)->setDescription('Maximum of 12 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		//card 3
		$fields->addFieldToTab(
			'Root.Articles Tiles Section.Text',
		    TextareaField::create('Artiles_tiles_card3_details','Card 3 Label')->setMaxLength(47)->setRows(2)->setDescription('Maximum of 47 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.Articles Tiles Section.Text',
	   	 	TextField::create('Artiles_tiles_card3_anchor','Card 3 Link Label')->setMaxLength(12)->setDescription('Maximum of 12 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);
		//END of Articles Tiles SECTION


		//CTA SECTION
		// $fields->addFieldToTab(
		// 	'Root.Main',
		// 	new HeaderField(" ",'CTA SECTION',1),
		// 	'Content'
		// );
		
		$fields->addFieldToTab(
			'Root.CTA Section.Text',
		    TextareaField::create('CTA_home_caption_top','Label Top')->setMaxLength(47)->setRows(2)->setDescription('Maximum of 47 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.CTA Section.Text',
	   	 	TextField::create('CTA_home_main_caption','Main Label')->setMaxLength(25)->setDescription('Maximum of 25 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		$fields->addFieldToTab(
			'Root.CTA Section.Text',
	   	 	TextField::create('CTA_home_button_label','Button Label')->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

	   	$fields->addFieldToTab(
        	'Root.CTA Section.Text',
        	TextField::create('CTA_home_button_label_redirect_to','Redirect To')->setAttribute('placeholder','http://sample.com')->setAttribute('type','url')->setAttribute('pattern','https?://.+')->setAttribute('required','required'),
        	// 'Content'
        	''
        );
		//END OF CTA SECTION


		// FOOTER SECTION
        // $fields->addFieldToTab(
        //     'Root.Footer Section.Text',
        //     new HeaderField(" ",'FOOTER SECTION',1),
        //      'Content'
        // );
            
        // address
        // $fields->addFieldToTab(
        //     'Root.Footer Section.Text',
        //     TextField::create('Footer_address','Address'),
        //     // 'Content'
        //     ''
        // );

        // phone
        // $fields->addFieldToTab(
        //     'Root.Footer Section.Text',
        //     TextField::create('Footer_phone','Phone'),
        //     // 'Content'
        //     ''
        // );

        //email
        // $fields->addFieldToTab(
        //     'Root.Footer Section.Text',
        //     TextField::create('Footer_email','Email'),
        //     // 'Content'
        //     ''
        // );

        //facebook
        // $fields->addFieldToTab(
        //     'Root.Footer Section.Text',
        //     TextField::create('Footer_social_facebook_link','Facebook Link'),
        //     // 'Content'
        //     ''
        // );

        //tweeter
        // $fields->addFieldToTab(
            // 'Root.Footer Section.Text',
            // TextField::create('Footer_social_tweeter_link','Tweeter Link'),
            // 'Content'
            // ''
        // );

        //instagram
        // $fields->addFieldToTab(
        //     'Root.Footer Section.Text',
        //     TextField::create('Footer_social_instagram_link','Instagram Link'),
        //     // 'Content'
        //     ''
        // );
        // END OF FOOTER SECTION


        // *****************************************

        //				END OF TEXT

        //******************************************




        // *****************************************

        //					IMAGE

        //******************************************


        //HEADER SECTION

        //Logo Top
        $fields->addFieldToTab(
            'Root.Header Section.Image',
            $Logo = UploadField::create('Logo','Logo')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 182px and height: 24px)"),
            // 'Content'
            ''
        );
        $Logo->getValidator()->setAllowedExtensions(['png','PNG']);
        $Logo->getValidator()->setMinDimensions(182,24);
        $Logo->setAllowedMaxFileNumber(1);
        $Logo->setFolderName('Uploads/Logo_Top');


        // Header_background
        $fields->addFieldToTab(
            'Root.Header Section.Image',
            $Header_background = UploadField::create('Header_background','Header Background')->setDescription("Only PNG, png, JPG, jpg, JPEG, jpeg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1905px and height: 869px)"),
            // 'Content'
            ''
        );
        $Header_background->getValidator()->setAllowedExtensions(['png','PNG','JPG','jpg','JPEG','jpeg']);
        $Header_background->getValidator()->setMinDimensions(1905,869);
        $Header_background->setAllowedMaxFileNumber(1);
        $Header_background->setFolderName('Uploads/Header_Background');

        //END OF HEADER SECTION





        //SERVICES SECTION

        //DESIGN
		$fields->addFieldToTab(
            'Root.Services Section.Image',
            $Design_pic = UploadField::create('Design_pic','Card Top Left')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 100px and height: 100px)"),
            // 'Content'
            ''
        );
        $Design_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Design_pic->getValidator()->setMinDimensions(100,100);
        $Design_pic->setAllowedMaxFileNumber(1);
        $Design_pic->setFolderName('Uploads/Our_Services_Background');

        //Strategy
        $fields->addFieldToTab(
            'Root.Services Section.Image',
            $Strategy_pic = UploadField::create('Strategy_pic','Card Top Right')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 100px and height: 100px)"),
            // 'Content'
            ''
        );
        $Strategy_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Strategy_pic->getValidator()->setMinDimensions(100,100);
        $Strategy_pic->setAllowedMaxFileNumber(1);
        $Strategy_pic->setFolderName('Uploads/Our_Services_Background');

        //Development Accelerator
        $fields->addFieldToTab(
            'Root.Services Section.Image',
            $Development_accelerator_pic = UploadField::create('Development_accelerator_pic','Card Bottom Left')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 100px and height: 100px)"),
            // 'Content'
            ''
        );
        $Development_accelerator_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Development_accelerator_pic->getValidator()->setMinDimensions(100,100);
        $Development_accelerator_pic->setAllowedMaxFileNumber(1);
        $Development_accelerator_pic->setFolderName('Uploads/Our_Services_Background');

        // consulting
        $fields->addFieldToTab(
            'Root.Services Section.Image',
            $Consulting_pic = UploadField::create('Consulting_pic','Card Bottom Right')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 100px and height: 100px)"),
            // 'Content'
            ''
        );
        $Consulting_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Consulting_pic->getValidator()->setMinDimensions(100,100);
        $Consulting_pic->setAllowedMaxFileNumber(1);
        $Consulting_pic->setFolderName('Uploads/Our_Services_Background');

        //END OF SERVICES SECTION




        //FEATURED WORKS
        

        //card 1 bg
        $fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_Card1_pic_background = UploadField::create('Featured_works_Card1_pic_background','Card 1 Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1110px and height: 323px)"),
            // 'Content'
            ''
        );
        $Featured_works_Card1_pic_background->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_Card1_pic_background->getValidator()->setMinDimensions(1110,323);
        $Featured_works_Card1_pic_background->setAllowedMaxFileNumber(1);
        $Featured_works_Card1_pic_background->setFolderName('Uploads/Featured_Background_Works');


        //card 1 right pic
		$fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_Card1_pic = UploadField::create('Featured_works_Card1_pic','Card 1 Right')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 727px and height: 1077px)"),
            // 'Content'
            ''
        );
        $Featured_works_Card1_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_Card1_pic->getValidator()->setMinDimensions(727,1077);
        $Featured_works_Card1_pic->setAllowedMaxFileNumber(1);
        $Featured_works_Card1_pic->setFolderName('Uploads/Featured_Works_Pic');


        //card 2 bg
        $fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_Card2_pic_background = UploadField::create('Featured_works_Card2_pic_background','Card 2 Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1110px and height: 323px)"),
            // 'Content'
            ''
        );
        $Featured_works_Card2_pic_background->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_Card2_pic_background->getValidator()->setMinDimensions(1110,323);
        $Featured_works_Card2_pic_background->setAllowedMaxFileNumber(1);
        $Featured_works_Card2_pic_background->setFolderName('Uploads/Featured_Background_Works');


        //card 2 right pic
        $fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_Card2_pic = UploadField::create('Featured_works_Card2_pic','Card 2 Right')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 727px and height: 1077px)"),
            // 'Content'
            ''
        );
        $Featured_works_Card2_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_Card2_pic->getValidator()->setMinDimensions(727,1077);
        $Featured_works_Card2_pic->setAllowedMaxFileNumber(1);
        $Featured_works_Card2_pic->setFolderName('Uploads/Featured_Works_Pic');
        //END OF FEATURED WORKS


        //Who We Are

        // Who_we_are_top_left
        $fields->addFieldToTab(
            'Root.Who We Are Section.Image',
            $Who_we_are_top_left_pic = UploadField::create('Who_we_are_top_left_pic','Top Left')->setDescription("Only PNG, png, JPG, jpg, JPEG, jpeg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 257px and height: 249px)"),
            // 'Content'
            ''
        );
        $Who_we_are_top_left_pic->getValidator()->setAllowedExtensions(['png','PNG','JPG','jpg','JPEG','jpeg']);
        $Who_we_are_top_left_pic->getValidator()->setMinDimensions(257,249);
        $Who_we_are_top_left_pic->setAllowedMaxFileNumber(1);
        $Who_we_are_top_left_pic->setFolderName('Uploads/Who_We_Are_Pic');



        //Who we are Top right
        $fields->addFieldToTab(
            'Root.Who We Are Section.Image',
            $Who_we_are_top_right_pic = UploadField::create('Who_we_are_top_right_pic','Top Right')->setDescription("Only PNG, png, JPG, jpg, JPEG, jpeg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 257px and height: 249px)"),
            // 'Content'
            ''
        );
        $Who_we_are_top_right_pic->getValidator()->setAllowedExtensions(['png','PNG','JPG','jpg','JPEG','jpeg']);
        $Who_we_are_top_right_pic->getValidator()->setMinDimensions(257,249);
        $Who_we_are_top_right_pic->setAllowedMaxFileNumber(1);
        $Who_we_are_top_right_pic->setFolderName('Uploads/Who_We_Are_Pic');



        //Who we are Bottom Left
        $fields->addFieldToTab(
            'Root.Who We Are Section.Image',
            $Who_we_are_bottom_left_pic = UploadField::create('Who_we_are_bottom_left_pic','Bottom Left')->setDescription("Only PNG, png, JPG, jpg, JPEG, jpeg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 257px and height: 249px)"),
            // 'Content'
            ''
        );
        $Who_we_are_bottom_left_pic->getValidator()->setAllowedExtensions(['png','PNG','JPG','jpg','JPEG','jpeg']);
        $Who_we_are_bottom_left_pic->getValidator()->setMinDimensions(257,249);
        $Who_we_are_bottom_left_pic->setAllowedMaxFileNumber(1);
        $Who_we_are_bottom_left_pic->setFolderName('Uploads/Who_We_Are_Pic');


        //Who we are right pic
        $fields->addFieldToTab(
            'Root.Who We Are Section.Image',
            $Who_we_are_bottom_right_pic = UploadField::create('Who_we_are_bottom_right_pic','Bottom Right')->setDescription("Only PNG, png, JPG, jpg, JPEG, jpeg are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 257px and height: 249px)"),
            // 'Content'
            ''
        );
        $Who_we_are_bottom_right_pic->getValidator()->setAllowedExtensions(['png','PNG','JPG','jpg','JPEG','jpeg']);
        $Who_we_are_bottom_right_pic->getValidator()->setMinDimensions(257,249);
        $Who_we_are_bottom_right_pic->setAllowedMaxFileNumber(1);
        $Who_we_are_bottom_right_pic->setFolderName('Uploads/Who_We_Are_Pic');
        //END of Who We ARE




        //OUR APPROACH


        // 1
        $fields->addFieldToTab(
            'Root.Our Approach Section.Image',
            $Our_Approach_pic_1 = UploadField::create('Our_Approach_pic_1','First')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 182px and height: 182px)"),
            // 'Content'
            ''
        );
        $Our_Approach_pic_1->getValidator()->setAllowedExtensions(['png','PNG']);
        $Our_Approach_pic_1->getValidator()->setMinDimensions(182,182);
        $Our_Approach_pic_1->setAllowedMaxFileNumber(1);
        $Our_Approach_pic_1->setFolderName('Uploads/Our_Approach');


        //2
        $fields->addFieldToTab(
            'Root.Our Approach Section.Image',
            $Our_Approach_pic_2 = UploadField::create('Our_Approach_pic_2','Second')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 182px and height: 182px)"),
            // 'Content'
            ''
        );
        $Our_Approach_pic_2->getValidator()->setAllowedExtensions(['png','PNG']);
        $Our_Approach_pic_2->getValidator()->setMinDimensions(182,182);
        $Our_Approach_pic_2->setAllowedMaxFileNumber(1);
        $Our_Approach_pic_2->setFolderName('Uploads/Our_Approach');


        //3
        $fields->addFieldToTab(
            'Root.Our Approach Section.Image',
            $Our_Approach_pic_3 = UploadField::create('Our_Approach_pic_3','Third')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 182px and height: 182px)"),
            // 'Content'
            ''
        );
        $Our_Approach_pic_3->getValidator()->setAllowedExtensions(['png','PNG']);
        $Our_Approach_pic_3->getValidator()->setMinDimensions(182,182);
        $Our_Approach_pic_3->setAllowedMaxFileNumber(1);
        $Our_Approach_pic_3->setFolderName('Uploads/Our_Approach');


        //4
        $fields->addFieldToTab(
            'Root.Our Approach Section.Image',
            $Our_Approach_pic_4 = UploadField::create('Our_Approach_pic_4','Fourth')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 182px and height: 182px)"),
            // 'Content'
            ''
        );
        $Our_Approach_pic_4->getValidator()->setAllowedExtensions(['png','PNG']);
        $Our_Approach_pic_4->getValidator()->setMinDimensions(182,182);
        $Our_Approach_pic_4->setAllowedMaxFileNumber(1);
        $Our_Approach_pic_4->setFolderName('Uploads/Our_Approach');
        //END OF OUR APPROACH




        // ARTICLE TILES


        //1
        $fields->addFieldToTab(
            'Root.Articles Tiles Section.Image',
            $Article_pic_1 = UploadField::create('Article_pic_1','First Tile Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 700px and height: 424px)"),
            // 'Content'
            ''
        );
        $Article_pic_1->getValidator()->setAllowedExtensions(['png','PNG']);
        $Article_pic_1->getValidator()->setMinDimensions(700,424);
        $Article_pic_1->setAllowedMaxFileNumber(1);
        $Article_pic_1->setFolderName('Uploads/Article_Tiles');


        //2
        $fields->addFieldToTab(
            'Root.Articles Tiles Section.Image',
            $Article_pic_2 = UploadField::create('Article_pic_2','Second Tile Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 700px and height: 424px)"),
            // 'Content'
            ''
        );
        $Article_pic_2->getValidator()->setAllowedExtensions(['png','PNG']);
        $Article_pic_2->getValidator()->setMinDimensions(700,424);
        $Article_pic_2->setAllowedMaxFileNumber(1);
        $Article_pic_2->setFolderName('Uploads/Article_Tiles');


        //3
        $fields->addFieldToTab(
            'Root.Articles Tiles Section.Image',
            $Article_pic_3 = UploadField::create('Article_pic_3','Third Tile Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 700px and height: 424px)"),
            // 'Content'
            ''
        );
        $Article_pic_3->getValidator()->setAllowedExtensions(['png','PNG']);
        $Article_pic_3->getValidator()->setMinDimensions(700,424);
        $Article_pic_3->setAllowedMaxFileNumber(1);
        $Article_pic_3->setFolderName('Uploads/Article_Tiles');

        
        //END OF ARTICLE TILES



        //CTA
        $fields->addFieldToTab(
            'Root.CTA Section.Image',
            $CTA_home_pic_background = UploadField::create('CTA_home_pic_background','Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1440px and height: 500px)"),
            // 'Content'
            ''
        );
        $CTA_home_pic_background->getValidator()->setAllowedExtensions(['png','PNG']);
        $CTA_home_pic_background->getValidator()->setMinDimensions(1440,500);
        $CTA_home_pic_background->setAllowedMaxFileNumber(1);
        $CTA_home_pic_background->setFolderName('Uploads/CTA');
        //END OF CTA



        //Footer Logo
		// $fields->addFieldToTab(
  //           'Root.Footer Section.Image',
  //           $Footer_Logo = UploadField::create('Footer_Logo','Logo')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 180px and height: 24px)"),
  //           // 'Content'
  //           ''
  //       );
  //       $Footer_Logo->getValidator()->setAllowedExtensions(['png','PNG']);
  //       $Footer_Logo->getValidator()->setMinDimensions(180,24);
  //       $Footer_Logo->setAllowedMaxFileNumber(1);
  //       $Footer_Logo->setFolderName('Uploads/Footer');
        //End of Footer Logo



        // *****************************************

        //				END OF IMAGE

        //******************************************



		// remove Static Content & meta data Fields
		$fields->removeFieldFromTab(
			'Root.Main',
			'Content'
		);

		$fields->removeFieldFromTab(
			'Root.Main',
			'Metadata'
		);

	    return $fields;

	}


}