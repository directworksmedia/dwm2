<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\View\Requirements;

    use SilverStripe\ORM\DataObject;

    // use Page;

    use Main\HomePage;


    class PageController extends ContentController
    {
        /**
         * An array of actions that can be accessed via a request. Each array element should be an action name, and the
         * permissions or conditions required to allow the user to access it.
         *
         * <code>
         * [
         *     'action', // anyone can access this action
         *     'action' => true, // same as above
         *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
         *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
         * ];
         * </code>
         *
         * @var array
         */
        private static $allowed_actions = [];

        protected function init()
        {
            parent::init();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/

            Requirements::css("assets2/node_modules/bootstrap/css/bootstrap.min.css");
            Requirements::css("assets2/node_modules/aos/dist/aos.css");
            Requirements::css("assets2/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.css");
            Requirements::css("assets2/node_modules/owl.carousel/dist/assets/owl.theme.green.css");

            Requirements::css("assets2/css/demo.css");
            Requirements::css("assets2/css/style.css");


            Requirements::javascript("assets2/node_modules/jquery/dist/jquery.min.js");
            Requirements::javascript("assets2/node_modules/popper/dist/popper.min.js");
            Requirements::javascript("assets2/node_modules/bootstrap/js/bootstrap.min.js");
            Requirements::javascript("assets2/node_modules/aos/dist/aos.js");
            Requirements::javascript("assets2/js/custom.min.js");
            Requirements::javascript("assets2/node_modules/jquery.touchSwipe.min.js");
            Requirements::javascript("assets2/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.js");
            Requirements::javascript("assets2/node_modules/owl.carousel/dist/owl.carousel.min.js");
            Requirements::javascript("assets2/js/isotope.pkgd.min.js");
            Requirements::javascript("assets2/js/portfolio.js");


        }


<<<<<<< HEAD
        public function GetFooter(){
=======
        // public function GetFooter(){

        //     return DataObject::get_one("Footer");
            
        // } 

        // public function Footer_address(){
        //     $obj = HomePage::get();

        //     return $obj['record']->Footer_address;
        // }


        // public function Footer_phone(){
        //     $obj = HomePage::get();
        //     return $obj['record']->Footer_phone;
        // }
>>>>>>> 0ab8b6d96389c3c4bfd6e38384fa89524f4e9921

            return DataObject::get_one("Footer");
            
        }

        public function GetHeader(){

            return HomePage::get();

        }

        // global functions
        public function CheckStringSpaces($string_to_check) {
              if(!empty(trim($string_to_check))) {
                 return true; 
                // return $string;
              } 
              return false;
        }


        

    }
}
