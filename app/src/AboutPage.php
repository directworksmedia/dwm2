<?php


namespace Main;

use Page;
//form fields
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\HeaderField;

// image and files fields
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;


use SilverStripe\Forms\HTMLEditor\HTMLEditorField;


class AboutPage extends Page
{

	private static $table_name = 'Main_AboutPage';

    //to force that any page created in about should only gallery page
    private static $allowed_children = [
        'GalleryPage',
    ];

	private static $db = [

		//****************

		//	TEXT

		//****************


		// IP Banner
		'IP_Banner' => 'Varchar(15)',
		//End Of IP Banner


        //Gallery Caption
        'Gallery_caption' => 'Varchar(30)',
        'Gallery_detail' => 'Varchar(100)',
        //End of Gallery Caption






		//we understand
		'We_understand_caption' => 'Varchar(35)',
		'We_understand_detail' => 'Varchar(100)',
		//end of we understand

        //Tools We Used
        'Tools_we_used_label' => 'Varchar(23)',


        //sketch
        'sketch_caption' => 'Varchar(15)',

        //marvel
        'marvel_caption' => 'Varchar(15)',

        //photoshop
        'photoshop_caption_caption' => 'Varchar(15)',

        //draw.io
        'draw_io_caption' => 'Varchar(15)',

        //trello
        'trello_caption' => 'Varchar(15)',

        //zeplin
        'zeplin_caption' => 'Varchar(15)',
        //End of tools we used



		//CTA Section
		'CTA_home_caption_top' => 'Varchar(47)',
		'CTA_home_main_caption' => 'Varchar(25)',
		'CTA_home_button_label' => 'Varchar(15)',
        'CTA_home_button_label_redirect_to' => 'Text',

		//END OF CTA Section




		//****************

		//	END OF TEXT

		//****************

	];


	private static $has_one = [

		//****************

		//	IMAGE

		//****************


		// IP Banner
		'IP_Banner_pic' => Image::class,
		//End Of IP Banner


		//we understand
		'We_understand_icon' => Image::class,
		'We_understand_bg' => Image::class,
		//end of we understand


        //tools we used
        'sketch_pic' => Image::class,
        'marvel_pic' => Image::class,
        'photoshop_pic' => Image::class,
        'draw_io_pic' => Image::class,
        'trello_pic' => Image::class,
        'zeplin_pic' => Image::class,
        //end of tools we used


		//CTA
		'CTA_home_pic_background' => Image::class,
        //END OF CTA





		//****************

		//	END OF IMAGE

		//****************

	];


	private static $owns = [

		// IP Banner
		'IP_Banner_pic',
		//End Of IP Banner

		//we understand
		'We_understand_icon',
		'We_understand_bg',
		//end of we understand


        //tools we used
        'sketch_pic',
        'marvel_pic',
        'photoshop_pic',
        'draw_io_pic',
        'trello_pic',
        'zeplin_pic',
        //end of tools we used


		//CTA
		'CTA_home_pic_background',
        //END OF CTA




	];

	public function getCMSFields(){

	    $fields = parent::getCMSFields();


	    //****************

		//	TEXT

		//****************


		//IP Banner
	    $fields->addFieldToTab(
			'Root.IP Banner Section.Text',
   	 		TextField::create('IP_Banner',"Label")->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
   	 		// 'Content'
   	 		''
   	 	);
        //End of IP Banner




        //Gallery Caption
        $fields->addFieldToTab(
            'Root.Gallery Section.Text',
            TextField::create('Gallery_caption',"Label")->setMaxLength(30)->setDescription('Maximum of 30 characters including spaces.'),
            // 'Content'
            ''
        );


        $fields->addFieldToTab(
            'Root.Gallery Section.Text',
            TextareaField::create('Gallery_detail',"Details")->setMaxLength(100)->setDescription('Maximum of 100 characters including spaces.')->setRows(2),
            // 'Content'
            ''
        );
        //End of Gallery Caption




        //we understand
		$fields->addFieldToTab(
			'Root.We Understand Section.Text',
   	 		TextField::create('We_understand_caption',"Label")->setMaxLength(35)->setDescription('Maximum of 35 characters including spaces.'),
   	 		// 'Content'
   	 		''
   	 	);

   	 	$fields->addFieldToTab(
			'Root.We Understand Section.Text',
		    TextareaField::create('We_understand_detail','Details')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );
        //end of we understand


        //tools we used
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Text',
            TextField::create('Tools_we_used_label',"Label")->setMaxLength(23)->setDescription('Maximum of 23 characters including spaces.'),
            // 'Content'
            ''
        );

        //sketch
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Text',
            TextField::create('sketch_caption',"Sketch")->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
            // 'Content'
            ''
        );

        //marvel
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Text',
            TextField::create('marvel_caption',"Marvel")->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
            // 'Content'
            ''
        );

        //photoshop
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Text',
            TextField::create('photoshop_caption_caption',"Photoshop")->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
            // 'Content'
            ''
        );

        //draw.io
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Text',
            TextField::create('draw_io_caption',"Draw.io")->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
            // 'Content'
            ''
        );

        //trello
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Text',
            TextField::create('trello_caption',"Trello")->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
            // 'Content'
            ''
        );

        //zeplin
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Text',
            TextField::create('zeplin_caption',"Zeplin")->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
            // 'Content'
            ''
        );

        //CTA SECTION
		// $fields->addFieldToTab(
		// 	'Root.Main',
		// 	new HeaderField(" ",'CTA SECTION',1),
		// 	'Content'
		// );
		
		$fields->addFieldToTab(
			'Root.CTA Section.Text',
		    TextareaField::create('CTA_home_caption_top','Label Top')->setMaxLength(47)->setRows(2)->setDescription('Maximum of 47 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.CTA Section.Text',
	   	 	TextField::create('CTA_home_main_caption','Main Label')->setMaxLength(25)->setDescription('Maximum of 25 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		$fields->addFieldToTab(
			'Root.CTA Section.Text',
	   	 	TextField::create('CTA_home_button_label','Button Label')->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

        $fields->addFieldToTab(
            'Root.CTA Section.Text',
            TextField::create('CTA_home_button_label_redirect_to','Redirect To')->setAttribute('placeholder','http://sample.com')->setAttribute('type','url')->setAttribute('pattern','https?://.+')->setAttribute('required','required'),
            // 'Content'
            ''
        );
        
		//END OF CTA SECTION

		//****************

		//	END OF TEXT

		//****************




		//****************

		//	IMAGE

		//****************



		//IP Banner
		$fields->addFieldToTab(
            'Root.IP Banner Section.Image',
            $IP_Banner_pic = UploadField::create('IP_Banner_pic','Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1440px and height: 394px)"),
            // 'Content'
            ''
        );
        $IP_Banner_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $IP_Banner_pic->getValidator()->setMinDimensions(1440,394);
        $IP_Banner_pic->setAllowedMaxFileNumber(1);
        $IP_Banner_pic->setFolderName('Uploads/IP_Banner');
        //End of IP Banner






        //we understand

        //icon
        $fields->addFieldToTab(
            'Root.We Understand Section.Image',
            $We_understand_icon = UploadField::create('We_understand_icon','Icon')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 120px and height: 120px)"),
            // 'Content'
            ''
        );
        $We_understand_icon->getValidator()->setAllowedExtensions(['png','PNG']);
        $We_understand_icon->getValidator()->setMinDimensions(120,120);
        $We_understand_icon->setAllowedMaxFileNumber(1);
        $We_understand_icon->setFolderName('Uploads/We_understand_icon');

        //background

        $fields->addFieldToTab(
            'Root.We Understand Section.Image',
            $We_understand_bg = UploadField::create('We_understand_bg','Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1454px and height: 479px)"),
            // 'Content'
            ''
        );
        $We_understand_bg->getValidator()->setAllowedExtensions(['png','PNG']);
        $We_understand_bg->getValidator()->setMinDimensions(1454,479);
        $We_understand_bg->setAllowedMaxFileNumber(1);
        $We_understand_bg->setFolderName('Uploads/We_understand_bg');
        //end of we understand




        //tools we used

        //sketch
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Image',
            $sketch_pic = UploadField::create('sketch_pic','Sketch')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 120px and height: 120px)"),
            // 'Content'
            ''
        );
        $sketch_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $sketch_pic->getValidator()->setMinDimensions(120,120);
        $sketch_pic->setAllowedMaxFileNumber(1);
        $sketch_pic->setFolderName('Uploads/Tools_We_Used');


        //marvel
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Image',
            $marvel_pic = UploadField::create('marvel_pic','Marvel')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 120px and height: 120px)"),
            // 'Content'
            ''
        );
        $marvel_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $marvel_pic->getValidator()->setMinDimensions(120,120);
        $marvel_pic->setAllowedMaxFileNumber(1);
        $marvel_pic->setFolderName('Uploads/Tools_We_Used');

        //photoshop
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Image',
            $photoshop_pic = UploadField::create('photoshop_pic','Photoshop')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 120px and height: 120px)"),
            // 'Content'
            ''
        );
        $photoshop_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $photoshop_pic->getValidator()->setMinDimensions(120,120);
        $photoshop_pic->setAllowedMaxFileNumber(1);
        $photoshop_pic->setFolderName('Uploads/Tools_We_Used');

        //draw.io
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Image',
            $draw_io_pic = UploadField::create('draw_io_pic','Draw.io')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 120px and height: 120px)"),
            // 'Content'
            ''
        );
        $draw_io_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $draw_io_pic->getValidator()->setMinDimensions(120,120);
        $draw_io_pic->setAllowedMaxFileNumber(1);
        $draw_io_pic->setFolderName('Uploads/Tools_We_Used');

        //trello
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Image',
            $trello_pic = UploadField::create('trello_pic','Trello')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 120px and height: 120px)"),
            // 'Content'
            ''
        );
        $trello_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $trello_pic->getValidator()->setMinDimensions(120,120);
        $trello_pic->setAllowedMaxFileNumber(1);
        $trello_pic->setFolderName('Uploads/Tools_We_Used');

        //zeplin
        $fields->addFieldToTab(
            'Root.Tools We Used Section.Image',
            $zeplin_pic = UploadField::create('zeplin_pic','Zeplin')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 120px and height: 120px)"),
            // 'Content'
            ''
        );
        $zeplin_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $zeplin_pic->getValidator()->setMinDimensions(120,120);
        $zeplin_pic->setAllowedMaxFileNumber(1);
        $zeplin_pic->setFolderName('Uploads/Tools_We_Used');
        //end of tools we used





        //CTA
        $fields->addFieldToTab(
            'Root.CTA Section.Image',
            $CTA_home_pic_background = UploadField::create('CTA_home_pic_background','Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1440px and height: 500px)"),
            // 'Content'
            ''
        );
        $CTA_home_pic_background->getValidator()->setAllowedExtensions(['png','PNG']);
        $CTA_home_pic_background->getValidator()->setMinDimensions(1440,500);
        $CTA_home_pic_background->setAllowedMaxFileNumber(1);
        $CTA_home_pic_background->setFolderName('Uploads/CTA');
        //END OF CTA
        
		//****************

		//	END OF IMAGE

		//****************



		// remove Static Content & meta data Fields
		$fields->removeFieldFromTab(
			'Root.Main',
			'Content'
		);

		$fields->removeFieldFromTab(
			'Root.Main',
			'Metadata'
		);


	    return $fields;


	}


}