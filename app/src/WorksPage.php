<?php


namespace Main;

use Page;
//form fields
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\HeaderField;

// image and files fields
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;


use SilverStripe\Forms\HTMLEditor\HTMLEditorField;



class WorksPage extends Page
{

	private static $table_name = 'Main_WorksPage';

	private static $db = [

		//****************

		//	TEXT

		//****************


		// IP Banner
		'IP_Banner' => 'Varchar(15)',
		//End Of IP Banner


		//Featured Works
		'Featured_works_caption' => 'Varchar(21)',

		'Featured_works_card1_caption' => 'Varchar(21)',
		'Featured_works_card1_detail' => 'Varchar(100)',
		'Featured_works_card1_sub' => 'Varchar(21)',

		'Featured_works_card2_caption' => 'Varchar(21)',
		'Featured_works_card2_detail' => 'Varchar(100)',
		'Featured_works_card2_sub' => 'Varchar(21)',


		'Featured_works_3column_1_card' => 'Varchar(20)',
		'Featured_works_3column_1_card_link' => 'Varchar(16)',

		'Featured_works_3column_2_card' => 'Varchar(20)',
		'Featured_works_3column_2_card_link' => 'Varchar(16)',

		'Featured_works_3column_3_card' => 'Varchar(20)',
		'Featured_works_3column_3_card_link' => 'Varchar(16)',


		//end of featured works




		//CTA Section
		'CTA_home_caption_top' => 'Varchar(47)',
		'CTA_home_main_caption' => 'Varchar(25)',
		'CTA_home_button_label' => 'Varchar(15)',
		'CTA_home_button_label_redirect_to' => 'Text',
		//END OF CTA Section




		//****************

		//	END OF TEXT

		//****************

	];


	private static $has_one = [

		//****************

		//	IMAGE

		//****************


		// IP Banner
		'IP_Banner_pic' => Image::class,
		//End Of IP Banner

		//Featured Works
		'Featured_works_Card1_pic_background' => Image::class,
		'Featured_works_Card1_pic' => Image::class,
		'Featured_works_Card2_pic_background' => Image::class,
		'Featured_works_Card2_pic' => Image::class,

		'Featured_works_3column_1_card_pic' => Image::class,
		'Featured_works_3column_2_card_pic' => Image::class,
		'Featured_works_3column_3_card_pic' => Image::class,

		//END OF Featured Works


		//CTA
		'CTA_home_pic_background' => Image::class,
        //END OF CTA



		//****************

		//	END OF IMAGE

		//****************

	];


	private static $owns = [

		// IP Banner
		'IP_Banner_pic',
		//End Of IP Banner


		//FEATURED WORKS
		'Featured_works_Card1_pic_background',
		'Featured_works_Card1_pic',
		'Featured_works_Card2_pic_background',
		'Featured_works_Card2_pic',


		'Featured_works_3column_1_card_pic',
		'Featured_works_3column_2_card_pic',
		'Featured_works_3column_3_card_pic',


        //END OF FEATURED WORKS


		//CTA
		'CTA_home_pic_background',
        //END OF CTA

	];

	public function getCMSFields(){

	    $fields = parent::getCMSFields();


	    //****************

		//	TEXT

		//****************


		//IP Banner
	    $fields->addFieldToTab(
			'Root.IP Banner Section.Text',
   	 		TextField::create('IP_Banner',"Label")->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
   	 		// 'Content'
   	 		''
   	 	);
        //End of IP Banner






	    // FEATURES WORKS SECTION
		// $fields->addFieldToTab(
		// 	'Root.Main',
		// 	new HeaderField(" ",'FEATURED WORKS SECTION',1),
		// 	'Content'
		// );

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_caption','Main Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);


		//First Board
		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_card1_caption','Card 1 Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
		    TextareaField::create('Featured_works_card1_detail','Card 1 Details')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_card1_sub','Card 1 Link Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);
		//END of First Board


		//Second Board
		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_card2_caption','Card 2 Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
		    TextareaField::create('Featured_works_card2_detail','Card 2 Details')->setMaxLength(100)->setRows(2)->setDescription('Maximum of 100 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_card2_sub','Card 2 Link Label')->setMaxLength(21)->setDescription('Maximum of 21 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);
		//END of Second Board



		// 3 column works


		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
			new HeaderField(" ",'3 Column Cards',1),
			// 'Content'
			''
		);


		//1
		$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_3column_1_card','Card 1 Label')->setMaxLength(20)->setDescription('Maximum of 20 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

	   	$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_3column_1_card_link','Card 1 Link Label')->setMaxLength(16)->setDescription('Maximum of 16 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);


	   	//2
	   	$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_3column_2_card','Card 2 Label')->setMaxLength(20)->setDescription('Maximum of 20 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

	   	$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_3column_2_card_link','Card 2 Link Label')->setMaxLength(16)->setDescription('Maximum of 16 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

	   	//3
	   	$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_3column_3_card','Card 3 Label')->setMaxLength(20)->setDescription('Maximum of 20 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

	   	$fields->addFieldToTab(
			'Root.Featured Works Section.Text',
	   	 	TextField::create('Featured_works_3column_3_card_link','Card 3 Link Label')->setMaxLength(16)->setDescription('Maximum of 16 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);



		// END OF FEATURES WORKS SECTION




      
        //CTA SECTION
		// $fields->addFieldToTab(
		// 	'Root.Main',
		// 	new HeaderField(" ",'CTA SECTION',1),
		// 	'Content'
		// );
		
		$fields->addFieldToTab(
			'Root.CTA Section.Text',
		    TextareaField::create('CTA_home_caption_top','Label Top')->setMaxLength(47)->setRows(2)->setDescription('Maximum of 47 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.CTA Section.Text',
	   	 	TextField::create('CTA_home_main_caption','Main Label')->setMaxLength(25)->setDescription('Maximum of 25 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		$fields->addFieldToTab(
			'Root.CTA Section.Text',
	   	 	TextField::create('CTA_home_button_label','Button Label')->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

	   	$fields->addFieldToTab(
        	'Root.CTA Section.Text',
        	TextField::create('CTA_home_button_label_redirect_to','Redirect To')->setAttribute('placeholder','http://sample.com')->setAttribute('type','url')->setAttribute('pattern','https?://.+')->setAttribute('required','required'),
        	// 'Content'
        	''
        );

        
		//END OF CTA SECTION


		//****************

		//	END OF TEXT

		//****************




		//****************

		//	IMAGE

		//****************


		//IP Banner
		$fields->addFieldToTab(
            'Root.IP Banner Section.Image',
            $IP_Banner_pic = UploadField::create('IP_Banner_pic','Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1440px and height: 394px)"),
            // 'Content'
            ''
        );
        $IP_Banner_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $IP_Banner_pic->getValidator()->setMinDimensions(1440,394);
        $IP_Banner_pic->setAllowedMaxFileNumber(1);
        $IP_Banner_pic->setFolderName('Uploads/IP_Banner');
        //End of IP Banner





        //FEATURED WORKS
        

        //card 1 bg
        $fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_Card1_pic_background = UploadField::create('Featured_works_Card1_pic_background','Card 1 Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1110px and height: 323px)"),
            // 'Content'
            ''
        );
        $Featured_works_Card1_pic_background->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_Card1_pic_background->getValidator()->setMinDimensions(1110,323);
        $Featured_works_Card1_pic_background->setAllowedMaxFileNumber(1);
        $Featured_works_Card1_pic_background->setFolderName('Uploads/Featured_Background_Works');


        //card 1 right pic
		$fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_Card1_pic = UploadField::create('Featured_works_Card1_pic','Card 1 Right')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 727px and height: 1077px)"),
            // 'Content'
            ''
        );
        $Featured_works_Card1_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_Card1_pic->getValidator()->setMinDimensions(727,1077);
        $Featured_works_Card1_pic->setAllowedMaxFileNumber(1);
        $Featured_works_Card1_pic->setFolderName('Uploads/Featured_Works_Pic');


        //card 2 bg
        $fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_Card2_pic_background = UploadField::create('Featured_works_Card2_pic_background','Card 2 Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1110px and height: 323px)"),
            // 'Content'
            ''
        );
        $Featured_works_Card2_pic_background->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_Card2_pic_background->getValidator()->setMinDimensions(1110,323);
        $Featured_works_Card2_pic_background->setAllowedMaxFileNumber(1);
        $Featured_works_Card2_pic_background->setFolderName('Uploads/Featured_Background_Works');


        //card 2 right pic
        $fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_Card2_pic = UploadField::create('Featured_works_Card2_pic','Card 2 Right')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 727px and height: 1077px)"),
            // 'Content'
            ''
        );
        $Featured_works_Card2_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_Card2_pic->getValidator()->setMinDimensions(727,1077);
        $Featured_works_Card2_pic->setAllowedMaxFileNumber(1);
        $Featured_works_Card2_pic->setFolderName('Uploads/Featured_Works_Pic');


        $fields->addFieldToTab(
			'Root.Featured Works Section.Image',
			new HeaderField(" ",'3 Column Cards',1),
			// 'Content'
			''
		);


		//1
		$fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_3column_1_card_pic = UploadField::create('Featured_works_3column_1_card_pic','Card 1 Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 350px and height: 310px)"),
            // 'Content'
            ''
        );
        $Featured_works_3column_1_card_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_3column_1_card_pic->getValidator()->setMinDimensions(350,310);
        $Featured_works_3column_1_card_pic->setAllowedMaxFileNumber(1);
        $Featured_works_3column_1_card_pic->setFolderName('Uploads/3_Column_Card_Works');

        

		//2
        $fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_3column_2_card_pic = UploadField::create('Featured_works_3column_2_card_pic','Card 2 Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 350px and height: 310px)"),
            // 'Content'
            ''
        );
        $Featured_works_3column_2_card_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_3column_2_card_pic->getValidator()->setMinDimensions(350,310);
        $Featured_works_3column_2_card_pic->setAllowedMaxFileNumber(1);
        $Featured_works_3column_2_card_pic->setFolderName('Uploads/3_Column_Card_Works');


		//3
		$fields->addFieldToTab(
            'Root.Featured Works Section.Image',
            $Featured_works_3column_3_card_pic = UploadField::create('Featured_works_3column_3_card_pic','Card 3 Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 350px and height: 310px)"),
            // 'Content'
            ''
        );
        $Featured_works_3column_3_card_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $Featured_works_3column_3_card_pic->getValidator()->setMinDimensions(350,310);
        $Featured_works_3column_3_card_pic->setAllowedMaxFileNumber(1);
        $Featured_works_3column_3_card_pic->setFolderName('Uploads/3_Column_Card_Works');
        //END OF FEATURED WORKS






        //CTA
        $fields->addFieldToTab(
            'Root.CTA Section.Image',
            $CTA_home_pic_background = UploadField::create('CTA_home_pic_background','Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1440px and height: 500px)"),
            // 'Content'
            ''
        );
        $CTA_home_pic_background->getValidator()->setAllowedExtensions(['png','PNG']);
        $CTA_home_pic_background->getValidator()->setMinDimensions(1440,500);
        $CTA_home_pic_background->setAllowedMaxFileNumber(1);
        $CTA_home_pic_background->setFolderName('Uploads/CTA');
        //END OF CTA

		//****************

		//	END OF IMAGE

		//****************


		// remove Static Content & meta data Fields
		$fields->removeFieldFromTab(
			'Root.Main',
			'Content'
		);

		$fields->removeFieldFromTab(
			'Root.Main',
			'Metadata'
		);


	    return $fields;


	}


}