<?php


namespace Main;

use Page;
//form fields
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\HeaderField;

// image and files fields
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;


use SilverStripe\Forms\HTMLEditor\HTMLEditorField;



class ContactPage extends Page
{

	private static $table_name = 'Main_ContactPage';

	private static $db = [

		//****************

		//	TEXT

		//****************


		// IP Banner
		'IP_Banner' => 'Varchar(15)',
		//End Of IP Banner


		//CTA Section
		'CTA_home_caption_top' => 'Varchar(47)',
		'CTA_home_main_caption' => 'Varchar(25)',
		'CTA_home_button_label' => 'Varchar(15)',
		'CTA_home_button_label_redirect_to' => 'Text',
		//END OF CTA Section




		//****************

		//	END OF TEXT

		//****************

	];


	private static $has_one = [

		//****************

		//	IMAGE

		//****************


		// IP Banner
		'IP_Banner_pic' => Image::class,
		//End Of IP Banner


		//CTA
		'CTA_home_pic_background' => Image::class,
        //END OF CTA



		//****************

		//	END OF IMAGE

		//****************

	];


	private static $owns = [

		// IP Banner
		'IP_Banner_pic',
		//End Of IP Banner




		//CTA
		'CTA_home_pic_background',
        //END OF CTA

	];

	public function getCMSFields(){

	    $fields = parent::getCMSFields();


	    //****************

		//	TEXT

		//****************


		//IP Banner
	    $fields->addFieldToTab(
			'Root.IP Banner Section.Text',
   	 		TextField::create('IP_Banner',"Label")->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
   	 		// 'Content'
   	 		''
   	 	);
        //End of IP Banner


      
        //CTA SECTION
		// $fields->addFieldToTab(
		// 	'Root.Main',
		// 	new HeaderField(" ",'CTA SECTION',1),
		// 	'Content'
		// );
		
		$fields->addFieldToTab(
			'Root.CTA Section.Text',
		    TextareaField::create('CTA_home_caption_top','Label Top')->setMaxLength(47)->setRows(2)->setDescription('Maximum of 47 characters including spaces.'),
	        // 'Content'
	        ''
	    );

		$fields->addFieldToTab(
			'Root.CTA Section.Text',
	   	 	TextField::create('CTA_home_main_caption','Main Label')->setMaxLength(25)->setDescription('Maximum of 25 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);

		$fields->addFieldToTab(
			'Root.CTA Section.Text',
	   	 	TextField::create('CTA_home_button_label','Button Label')->setMaxLength(15)->setDescription('Maximum of 15 characters including spaces.'),
	   	 	// 'Content'
	   	 	''
	   	);


	   	$fields->addFieldToTab(
        	'Root.CTA Section.Text',
        	TextField::create('CTA_home_button_label_redirect_to','Redirect To')->setAttribute('placeholder','http://sample.com')->setAttribute('type','url')->setAttribute('pattern','https?://.+')->setAttribute('required','required'),
        	// 'Content'
        	''
        );
        
		//END OF CTA SECTION


		//****************

		//	END OF TEXT

		//****************




		//****************

		//	IMAGE

		//****************


		//IP Banner
		$fields->addFieldToTab(
            'Root.IP Banner Section.Image',
            $IP_Banner_pic = UploadField::create('IP_Banner_pic','Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1440px and height: 394px)"),
            // 'Content'
            ''
        );
        $IP_Banner_pic->getValidator()->setAllowedExtensions(['png','PNG']);
        $IP_Banner_pic->getValidator()->setMinDimensions(1440,394);
        $IP_Banner_pic->setAllowedMaxFileNumber(1);
        $IP_Banner_pic->setFolderName('Uploads/IP_Banner');
        //End of IP Banner


        //CTA
        $fields->addFieldToTab(
            'Root.CTA Section.Image',
            $CTA_home_pic_background = UploadField::create('CTA_home_pic_background','Background')->setDescription("Only PNG, png are allowed<br>2MB Maximum File Size<br>Image Dimension (width: 1440px and height: 500px)"),
            // 'Content'
            ''
        );
        $CTA_home_pic_background->getValidator()->setAllowedExtensions(['png','PNG']);
        $CTA_home_pic_background->getValidator()->setMinDimensions(1440,500);
        $CTA_home_pic_background->setAllowedMaxFileNumber(1);
        $CTA_home_pic_background->setFolderName('Uploads/CTA');
        //END OF CTA

		//****************

		//	END OF IMAGE

		//****************


		// remove Static Content & meta data Fields
		$fields->removeFieldFromTab(
			'Root.Main',
			'Content'
		);

		$fields->removeFieldFromTab(
			'Root.Main',
			'Metadata'
		);


	    return $fields;


	}


}