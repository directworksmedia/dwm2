<?php

use SilverStripe\ORM\DataObject;

use SilverStripe\Assets\Image;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;

use GalleryPage;

class Photo extends DataObject
{

	private static $db = [

        // Image Gallery TEXT
        'Gallery_caption' => 'Varchar(20)',
        // End of Image Gallery TEXT

	];


    private static $versioned_gridfield_extensions = true;


	private static $has_one = [
        'Photo' => Image::class,
        'GalleryPage' => GalleryPage::class,
    ];

    private static $owns = [
        'Photo',
    ];

    //custom field to view in table
    //the value is the "name of the column in table"
	private static $summary_fields = [
        'GridThumbnail' => 'Photo',
        'Gallery_caption' => 'Title',
    ];

    // return the value of GridThumbnail key in $summary_fields
    public function getGridThumbnail()
    {
        if($this->Photo()->exists()) {
            return $this->Photo()->ScaleWidth(100)->ScaleHeight(100);
        }
        return "( No image )";
    }


    public function getCMSFields()
    {
        $fields = FieldList::create(
            TextField::create('Gallery_caption','Title'),
            $uploader = UploadField::create('Photo','Image')
        );

        $uploader->setFolderName('Uploads/Gallery_Photos');
        $uploader->getValidator()->setAllowedExtensions(['png','jpeg','jpg']);

        return $fields;
    }


}