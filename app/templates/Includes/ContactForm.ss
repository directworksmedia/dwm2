                            <script src='https://www.google.com/recaptcha/api.js'></script>
                          
                           <div class="" data-aos="fade-right" data-aos-duration="1200">
                                    <form data-aos="fade-left" id = 'id_form_contact' data-aos-duration="1200" method="POST" action="$getCurrentLink">

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" placeholder="Name" name = 'name' required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <input class="form-control" type="email" placeholder="Email Address" name = 'email' required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" placeholder="Contact Number" name = 'contact' required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="3" placeholder="Your message" name = 'message' required></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <div class="g-recaptcha" data-sitekey="6Leo9n8UAAAAAIQ8UXmR0IbC74abW5GxbKi30vOf"></div>
                                            </div>

                                            <div class="d-flex mx-auto no-block m-t-20 text-center">
                                                <button type="submit" class="btn btn-pink btn-rounded btn-arrow" id = 'id_send_message'><span> Send message <i class="ti-arrow-right"></i></span></button>
                                            </div>
                                        </div>
                                        
                                        <div class="contact-alter">
                                            <p>or email us at <a href="#">hello@directworksmedia.com</a></p>
                                        </div>

                                    </form>
                            </div>


<script type="text/javascript">

    $('#id_form_contact').on('submit', function(e) {
        e.preventDefault();

      if(grecaptcha.getResponse() == "") {
        alert("Please check the recaptcha");
      } else {

            // do the saving
            var url = $('#id_form_contact').attr('action');
            var data  = $('#id_form_contact').serialize();

            $("#id_send_message").html("Please Wait...").attr('disabled',true);

            // $.ajax({
            //     url:"",
            //     type:"",
            //     data:"",
            //     beforesend:"",
            //     success:function(data){

            //     }
            // })

            $.post(url, data , function(response){

                console.log(response);

                if (response['valid'] == "email_not_send") {

                    alert("Emailing Problem , Please Try Again.");

                    // alert("Success");

                }else if (response['valid'] == "success"){

                    window.location = '/thankyou';

                }else {

                    alert(response['valid']);

                }

                $("#id_send_message").html("<span> Send message <i class='ti-arrow-right'></i></span>").attr('disabled',false);
                grecaptcha.reset();
                $('#id_form_contact')[0].reset();

            },"json");


      }
    });

</script>


